import navigation from "./navigation"
import signatureDialog from "./signatureDialog"

import { combineReducers } from 'redux'

export default combineReducers({
  navigation,
  signatureDialog
})
