import * as types from '../constants/ActionTypes';

const initialState = {
  modal: {
    signature: false
  },
  signatures: [],
  signaturesById: []
}

export default function modal( state = initialState, action ) {
  let newState

  switch ( action.type ) {

    case types.OPEN_MODAL:
      console.log( "OPEN_MODAL", action )
      newState = {
        ...state,
        modal: {
          ...state.modal,
          [action.name]: true,
        }
      }
      console.log( "newState", newState )
      return newState

    case types.CLOSE_MODAL:
      console.log( "CLOSE_MODAL", action )
      newState = {
        ...state,
        modal: {
          ...state.modal,
          [action.name]: false,
        }
      }

      console.log( "newState", newState )
      return newState

    case types.NEW_SIGNATURE:
      console.log( "NEW_SIGNATURE", action )
      let signatures = state.signatures
      let signaturesById = state.signaturesById
      newState = {
        ...state,
        signatures: [...signatures, action],
        signaturesById: [...signaturesById, action.id]
      }
      console.log( "newState", newState )
      return newState

    default:
      return state
  }
}
