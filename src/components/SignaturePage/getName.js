const nouns = ["car", "private airplane", "washing machine",  "Laser-Tag Palace"]
const adjectives0 = ["new", "pirated", "fancy", "humongous", "unused", "shiny" ]
const adjectives1 = ["red", "yellow", "painted", "picturesque", "plastic"]
const withExtra = ["including multiple sunroofs", "including unlimited tab water", "including DIN 5008 certification"]

function randomItem( list ) {
  return list[Math.floor( Math.random() * list.length )];
}

export default function getName() {

  let extra = ""
  if(Math.random()>0.5){
      extra = " " +randomItem(withExtra)
  }

  return randomItem( adjectives0 ) + ", " + randomItem( adjectives1 ) + " " + randomItem( nouns ) + extra
}
