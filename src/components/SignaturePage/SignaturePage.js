import React, { PropTypes, Component } from 'react'
import withStyles from '../../decorators/withStyles'
import styles from './SignaturePage.scss'

import {connect} from 'react-redux'
import {openModal, closeModal, saveSignature} from "../../actions/SignatureDialogActions"

import FlatButton from 'material-ui/lib/flat-button'
import RaisedButton from 'material-ui/lib/raised-button'

import SignatureDialog from "../SignatureDialog"


// demo content
import getName from "./getName"
var buyThisName  = getName()
var id = 0

@connect( state => (
  {
    open: state.signatureDialog.modal.signature,
    signatures: state.signatureDialog.signatures,
    signaturesById: state.signatureDialog.signaturesById,
  }),
  {
    openModal,
    closeModal,
    saveSignature
  },
  null,
  {withRef: false}
)
@withStyles( styles )
class SignaturePage extends Component {

  static contextTypes = {
    // provided by @withContext
    onSetTitle: PropTypes.func.isRequired,
  }

  static propTypes = {
    // provided by @connect
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    saveSignature: PropTypes.func.isRequired,
    signatures: PropTypes.array,
    signaturesById: PropTypes.array,
  }

  handleOpen = () => {
    this.props.openModal( "signature" )
  }

  handleClose = () => {
    this.props.closeModal( "signature" )
    buyThisName = getName()

  }

  onFail = ( error ) => {
    this.handleClose( error )

  }

  onSuccess = ( dataUrl ) => {
    console.info( "::onSuccess" )
    /* Aquire metadata:  in this case we just make it up on the spot */
    let name = buyThisName
    let taskId = 1321
    id = id + 1

    this.props.saveSignature( dataUrl, {id: id, name: name, taskId: taskId} )
    this.handleClose()

  }


  render() {
    const title = 'Signature Implementation'

    const {open} = this.props
    const {handleOpen, onSuccess, onFail} = this

    this.context.onSetTitle( title )

    const signatureOptions = {
      minWidth: 1,
      maxWidth: 3,
      penColor: "rgb(10,10,10)",
      encoderOptions: "image/jpeg",
      backgroundColor: "rgb(255,255,255)",
    }


    return (
      <div className="SignaturePage">
        <div className="SignaturePage-container">
          <div className="demo-ui">
            <p> Sign here to get a {buyThisName}</p>
            <RaisedButton label="SIGN IT" onTouchTap={handleOpen}/>
          </div>

          <SignatureDialog
            open={open}
            onSuccess={onSuccess}
            onFail={onFail}
            signatureOptions={signatureOptions}
          />

          <hr/>
          { this.props.signatures.map( ( item )=> {
            return (
              <div
                style={{float:"left", width:"320px", margin: "16px", padding:"5px", border: "1px solid #efefef"}}
              >
                <img key={item.id}
                     style={{width:"320px"}}
                     src={item.dataUrl}/>
                <br/>
                <p style={{textAlign:"center"}}> a {item.name}</p>
              </div>
            )
          } )}
        </div>
      </div>
    )
  }
}

export default SignaturePage
