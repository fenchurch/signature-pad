/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, {PropTypes, Component} from 'react';
import styles from './App.scss';
import withContext from '../../decorators/withContext';
import withStyles from '../../decorators/withStyles';
import Header from '../Header';
import Feedback from '../Feedback';
import Footer from '../Footer';
import Navigation from '../Navigation';
import MFRApp from "./MFRApp"

import { combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { createStore , compose } from 'redux';
import * as reducers from './../../reducers';

console.log( "reducserS", reducers )

import ThemeImplementation from '../../material-theme';
import ThemeManager from 'material-ui/lib/styles/theme-manager';
import ThemeDecorator from 'material-ui/lib/styles/theme-decorator';

const reducer = combineReducers( reducers );
const store = createStore( reducer );

@withContext
@withStyles( styles )
@ThemeDecorator( ThemeManager.getMuiTheme( ThemeImplementation ) )
class App extends Component {

  static propTypes = {
    children: PropTypes.element.isRequired,
    error: PropTypes.object
  };

  render() {
    return !this.props.error
      ? (
      <div>
        <Header/>

        <Navigation className="Header-nav"/>
        {this.props.children}


      </div>
    )
      : this.props.children;
  }

}

export default App;

