import React, {PropTypes, Component} from 'react'
import styles from './App.scss'
import withContext from '../../decorators/withContext'
import withStyles from '../../decorators/withStyles'

import { combineReducers } from 'redux'
import { Provider } from 'react-redux'
import { createStore , compose } from 'redux'
import  reducer from './../../reducers'

import ThemeImplementation from '../../material-theme'
import ThemeManager from 'material-ui/lib/styles/theme-manager'
import ThemeDecorator from 'material-ui/lib/styles/theme-decorator'

import SignatureDialog from "../SignatureDialog"

const store = createStore( reducer )

@withContext
@withStyles( styles )
@ThemeDecorator( ThemeManager.getMuiTheme( ThemeImplementation ) )
class App extends Component {

  static propTypes = {
    children: PropTypes.element.isRequired,
    error: PropTypes.object
  };

  render() {
    return !this.props.error
      ? (
      <div>
        <Provider store={store}>
          {this.props.children}
        </Provider>
      </div>
    )
      : this.props.children
  }

}

export default App;
