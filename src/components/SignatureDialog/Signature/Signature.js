import React, {PropTypes, Component} from 'react'
import Location from '../../../core/Location'
import withStyles from '../../../decorators/withStyles'

import RaisedButton from 'material-ui/lib/raised-button'
import FlatButton from 'material-ui/lib/flat-button'
import Paper from 'material-ui/lib/paper'


let signaturePad


function scaleCanvas( canvas ) {

  const DOMSignatureBody = document.querySelector( ".SignatureDialog__body" )
  const ratio = Math.max( window.devicePixelRatio || 1, 1 )

  canvas.width = DOMSignatureBody.offsetWidth * ratio
  canvas.height = DOMSignatureBody.offsetHeight * ratio
  canvas.getContext( "2d" ).scale( ratio, ratio )

  if ( signaturePad ) {
    signaturePad.clear()
  }
  return canvas
}

class Signature extends Component {

  static propTypes = {
    onPublish: PropTypes.func,
    penColor: PropTypes.string,
    encoderOptions: PropTypes.oneOf( ['image/jpeg', 'image/png'] ),
    publish: PropTypes.func,
    options: PropTypes.object,
  }

  static defaultProps = {

    onComplete: () => {
      console.info( "onComplete callback not set" )
    },
    options:{
      minWidth: 1,
      maxWidth: 3,
      penColor: "rgb(10,10,10)",
      encoderOptions: "image/jpeg",
      backgroundColor: "rgb(255,255,255)",
    }
  }

  publish = () => {

    var dataUrl = signaturePad.toDataURL( this.props.encoderOptions )

    return dataUrl
  }

  isEmpty = () => {

    return signaturePad.isEmpty()
  }

  handleResize = () => {

    const {canvas} = this.refs

    if ( signaturePad ) {
      signaturePad.clear()
    }

    window.setTimeout( () => {
      scaleCanvas( canvas )
    }, 350 )
  }

  componentDidMount() {
    window.addEventListener( "resize", this.handleResize )

    let SignaturePad = require( "./SignaturePad" )
    const canvas = this.refs.canvas

    scaleCanvas( canvas )
    signaturePad = new SignaturePad( canvas, this.props.options )
  }

  componentWillUnmount() {

    window.removeEventListener( "resize", this.handleResize )
    signaturePad.off()
  }

  render() {

    return (
      <div>
        <canvas ref="canvas"/>
        <br/>
        <div className="overlay-ui baseLine"></div>
        <div className="pull-right">
        </div>
      </div>
    )
  }

}

export default Signature
