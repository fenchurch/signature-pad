import React, {PropTypes, Component} from 'react'
import withStyles from '../../decorators/withStyles'

import Signature from "./Signature"
import styles from './SignatureDialog.scss'

import Dialog from 'material-ui/lib/dialog'
import FlatButton from 'material-ui/lib/flat-button'
import RaisedButton from 'material-ui/lib/raised-button'

@withStyles( styles )
class SignatureDialog extends Component {

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
    store: PropTypes.object,
  }

  static propTypes = {
    open: PropTypes.bool.isRequired,
    onFail: PropTypes.func.isRequired,
    onSuccess: PropTypes.func.isRequired,
    modal: PropTypes.bool,
    signatureOptions: PropTypes.object,
  }

  static  defaultProps = {
    modal: true,
  }

  handleSuccess = () => {

    const signature = this.refs.signature    // tightly coupled with child <Signature/>

    if ( signature.isEmpty() ) {
      this.props.onFail( "No signature entered" )
    } else {
      const dataUrl = signature.publish()
      this.props.onSuccess( dataUrl )
    }
  }

  render() {

    const {open, onFail, modal, signatureOptions} = this.props

    const modalActionsContainer = [
      <FlatButton label="Cancel" onTouchTap={onFail}/>,
      <RaisedButton onTouchTap={this.handleSuccess} label="Confirm" primary={true}/>
    ]


    return (
      <div className="SignatureDialog">
        <Dialog
          contentClassName="SignatureDialog__content"
          bodyClassName="SignatureDialog__body"
          actions={modalActionsContainer}
          modal={modal}
          open={open}>
          <Signature
            ref="signature"
            options={signatureOptions}
          />
        </Dialog>
      </div>
    )
  }
}

export default SignatureDialog
