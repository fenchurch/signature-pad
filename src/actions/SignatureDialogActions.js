import * as types from '../constants/ActionTypes';

export function openModal( name ) {
  return {
    type: types.OPEN_MODAL,
    name
  }
}

export function closeModal( name ) {
  return {
    type: types.CLOSE_MODAL,
    name
  }
}

export function saveSignature( dataUrl, options ) {
  let { name, id, taskId } = options
  return {
    type: types.NEW_SIGNATURE,
    dataUrl,
    name,
    id,
    taskId
  }
}
