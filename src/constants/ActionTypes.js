/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import keyMirror from 'fbjs/lib/keyMirror'

export default keyMirror({

})

export const ADD_FRIEND = 'ADD_FRIEND'
export const STAR_FRIEND = 'STAR_FRIEND'
export const DELETE_FRIEND = 'DELETE_FRIEND'

// SignatureDialogActions
export const OPEN_MODAL = 'OPEN_MODAL'
export const CLOSE_MODAL = 'CLOSE_MODAL'
export const NEW_SIGNATURE = 'NEW_SIGNATURE'
