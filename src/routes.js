/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React from 'react'
import Router from 'react-routing/src/Router'
import http from './core/HttpClient'
import App from './components/App'

import SignaturePage from './components/SignaturePage'

const router = new Router(on => {
	on('*', async(state, next) => {
		const component = await next()
		return component && <App context={state.context}>{component}</App>;
	})

	on('/', async() => <SignaturePage/>)

})

export default router
